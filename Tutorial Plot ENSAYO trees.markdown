---
title: "Plotting a 3D representation of the CATIE ENSAYO trial"
author: "VEZY R."
date: "Sunday, June 07, 2015"
output: html_document
status: publish
published: true
---
  
  
  
###This markdown explains how to plot the ENSAYO trial trees on a 3D WebGL plot using R.  
 
 
 
**Packages needed:** Maeswrap and its dependencies.  
**Data needed:** ENSAYO trial MAESPA input data (.dat) files available on this. [depository](https://drive.google.com/folderview?id=0Bx8raG_nUy29UmdkbTBNNTlaTFE&usp=sharing)
**Others:** updated version of the plotstand Maeswrap function (same depository).
 
 
First of all, set the working directory and call the package and the function:

    setwd("C:/Users/User Name/Desktop/ENSAYO_MAESPA_Data_Files")
    source("C:/Users/User Name/Desktop/ENSAYO_MAESPA_Data_Files/Maeswrap_cor_VR.R")
    library(Maeswrap)
 
Then, just call the function:
 

    PlotstandVR (treesfile = "Trees.dat", strfile = c("str1.dat","str2.dat", "str3.dat", "str4.dat"), 
                 confile='confile.dat',crownshape = "halfellipsoid",readstrfiles = TRUE, axiscex = 1,
                 targethighlight = TRUE, addNarrow = TRUE, xyaxes = TRUE, labcex = 1, verbose = TRUE,
                 MulticolorS=T, Date = 1, Title= "ENSAYO 3D STRUCTURE", Legend=T, Latin=T, Lcex=2, 
                 crowncolorchoose=c("forestgreen", "darkorange1", "cadetblue3", "burlywood4"))
 
An rgl window will pop up as a result, and the construction of the plot will start.  
Note that this operation can be very slow.
If you want to stop the operation, do NOT try to close the rglwindow. You first need to stop R process
directly from R, THEN you can close it.  
 
Here is a snapshot of the resulting plot (GIF and AVI files are also present on the depository)

    ## Error in readPNG("F:/These/Ensayo_CATIE/Representations/movie000.png", : unable to open F:/These/Ensayo_CATIE/Representations/movie000.png

![plot of chunk unnamed-chunk-3](/images/figure/unnamed-chunk-3-1.png) 

    ## Error in rasterImage(Image, 1, 1, 2, 2): object 'Image' not found
 
There is several ways to save the result:
 
* Take a snapshot of the scene:  
Move the plot with your mouse until you get the right angle, and then, just run this code:

    snapshot3d('ENSAYO_plot_Trees.png')
The resulting image will be saved in the working directory
 
* Save many images in order to process an animated GIF or a video file:  
The following function will rotate the plot at the "rpm" speed and take "fps" number of snapshot every second, during "duration" seconds.
Please, create a folder named images in the "ENSAYO_MAESPA_Data_Files".

    movie3d(spin3d(axis = c(0, 0, 1), rpm = 1, dev = rgl.cur(), subscene = currentSubscene3d()),
            duration= 60, dir= "C:/Users/User name/Desktop/ENSAYO_MAESPA_Data_Files/images", fps=24)
 
Then, you can process the resulting images in GIMP or imageJ(64) to make a movie.
You could also do that directly from R, assuming that you installed imageMagick on your computer, by adding
to the function the argument "convert = TRUE".
Unfortunately, this function require a lot of memory and computation time. I do not recommend it. 
 
* Save the plot directly into HTML format:

    writeWebGL(dir = "C:/Users/User name/Desktop/ENSAYO_MAESPA_Data_Files")
The results of this function are unpredictable though.
 
 
 
 
 
 
If you need help on the function's arguments:

    ?Plotstand
 
Also, **PlotstandVR** add these new arguments: crowncolorchoose, Legend, Latin and LegendManual  
 
* crowncolorchoose defines the colors of the crown or trunk of the species. 
Default= "forestgreen" (all same color)  
 
* Legend: if True, plot a legend with the species names and the color specified by crowncolorchoose
Default= F  
 
* Latin: if Legend=T and Latin= T, the species names are plotted in Italic (species names fixed)
Default=F  
 
* LegendManual: if Legend=F, vector which specify manually the names of the species.  
  
  
